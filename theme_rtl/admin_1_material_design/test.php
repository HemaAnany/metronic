<?php
/**
 * Created by PhpStorm.
 * User: win 10
 * Date: 5/10/2020
 * Time: 11:52 AM
 */


public function datatable(Request $request)
{
    $columns = [
        0 => 'image',
        1 => app()->getLocale().'_name',
        2 => 'products',
        3 => 'fields',
        4 => 'actions'
    ];

    $limit = $request->input('length');
    $offset = $request->input('start');
    $order = $columns[$request->input('order.0.column')];
    $dir   = $request->input('order.0.dir');

    $builder = Category::where('publish', 1)
        ->offset($offset)
        ->limit($limit)
        ->orderBy($order, $dir);

    $totalBuilder = Category::where('publish', 1)
        ->orderBy($order, $dir);

    $totalCount = Category::where('publish', 1)->count();

    if(!empty($request->input('search.value'))){
        $search = $request->input('search.value');
        $builder->where(app()->getLocale().'_name', 'like', '%'.$search.'%');

        $totalBuilder->where(app()->getLocale().'_name', 'like', '%'.$search.'%');
    }

    $result = $builder->get();

    $totalFiltered = $totalBuilder->count();
    $data = [];

    foreach($result AS $row){
        $data[] = [
            'image' => ($row->icon)? '<img src="'.url(constants('pathes.categories', $row->icon)).'" alt="'.$row->{app()->getLocale().'_name'}.'" title="'.$row->{app()->getLocale().'_name'}.'" style="max-width:80px" />' : '',
            'name' => $row->{app()->getLocale().'_name'},
            'fields' => $row->fields->count(),
            'products' => $row->products->count(),
            'actions' => '
                    <div class="btn-group btn-group-justified">
                        <a href="#" class="btn btn-primary" title="'.__('Preview').'">
                            <i class="fa fa-eye"></i>
                        </a>
                        <a href="'.route('category.edit', $row->slug).'" class="btn btn-success" title="'.__('Edit').'">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a href="#" class="btn btn-danger" title="'.__('Delete').'">
                            <i class="fa fa-trash"></i>
                        </a>
                    </div>
                '
        ];
    }


    $json_data = array(
        "draw"         => intval($request->input('draw')),
        "recordsTotal" => intval($totalCount),
        "recordsFiltered" => intval($totalFiltered),
        "data"         => $data
    );

    echo json_encode($json_data);
}